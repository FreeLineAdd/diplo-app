package com.example.admin.aplicationforcursaci;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import common.AbstractAction;
import common.AbstractActivity;
import common.RecyclerViewAdapter;

import static common.AbstractAction.toMap;

public class UserBoxActivity extends AbstractActivity {

    DatabaseReference mFirebaseDatabase = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null)
            throw new NullPointerException("User is not found");

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("USERS").child(user.getDisplayName() + ":" + user.getUid());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        activity = this;
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            fbLog = new FirebaseLogger(currentUser);
        } else
            throw new NullPointerException("User is not found");

        ChildEventListener valueEventListener = new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                HashMap<String, Object> orders = new HashMap<>();

                Stream.of(dataSnapshot).forEach(a -> orders.put(a.getKey(), toMap(a.getValue())));

                //Show ordersInBox
                if (orders.get("box") != null) {
                    List<HashMap<String, Object>> ordersInBox = toMap(orders.get("box")).values().stream().map(AbstractAction::toMap).collect(Collectors.toList());
                    mAdapter = new RecyclerViewAdapter(getBaseContext(), activity, ordersInBox, txt -> onClickTextView(txt), false, true);
                    recyclerView.setAdapter(mAdapter);
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        mFirebaseDatabase.addChildEventListener(valueEventListener);


    }


}
