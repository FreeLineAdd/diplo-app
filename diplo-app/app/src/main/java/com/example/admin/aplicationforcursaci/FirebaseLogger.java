/*
 # Copyright 2016 Google LLC.
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 # http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.
 */

package com.example.admin.aplicationforcursaci;

import android.annotation.SuppressLint;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * FirebaseLogger pushes user event logs to a specified path.
 * A backend servlet instance listens to
 * the same key and keeps track of event logs.
 */
public class FirebaseLogger {
    private final DatabaseReference logRef;

    @SuppressLint("SimpleDateFormat")
    public FirebaseLogger(FirebaseUser user) {
        logRef = FirebaseDatabase.getInstance().getReference().child("USERS").child(user.getDisplayName() + ":" + user.getUid()).child("logs").child(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void log(String tag, String message) {
        logRef.push().setValue(String.format("TAG=%s, %s - %s", tag, new SimpleDateFormat("HH:mm:ss").format(new Date()), message));
    }

}
