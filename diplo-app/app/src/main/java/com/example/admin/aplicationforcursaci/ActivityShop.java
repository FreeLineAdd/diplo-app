package com.example.admin.aplicationforcursaci;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import common.AbstractActivity;
import common.Products;
import common.RecyclerViewAdapter;


public class ActivityShop extends AbstractActivity {

    private static final String TAG = "ActivityShop";
    public final int MENU_BUTTON_ORDER = 112233;
    public final int MENU_BUTTON_BOX = 221133;
    DatabaseReference mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        activity = this;
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            fbLog = new FirebaseLogger(currentUser);
        } else
            throw new NullPointerException("User is not found");

//        ChildEventListener valueEventListener = new ChildEventListener() {
//
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                Stream.of(dataSnapshot).forEach(a -> products.put(a.getKey(), toMap(a.getValue())));
//
//                //Show collectionOfProducts
//
//                List<HashMap<String, Object>> list = new ArrayList<>();
//
//                for (HashMap<String, Object> innerMap : Arrays.asList(products.get("PRODUCTS").values().toArray()).stream().map(AbstractAction::toMap).collect(Collectors.toList())) {
//                    System.out.println();
//                    innerMap.forEach((a, b) -> {
//                        list.add(toMap(b));
//                    });
//                }
//
//                mAdapter = new RecyclerViewAdapter(getBaseContext(), activity, list, txt -> onClickTextView(txt), true, false);
//                recyclerView.setAdapter(mAdapter);
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        };
        while (!MainActivity.downloadTh.getState().equals(Thread.State.TERMINATED)) {
            sleep(100);
        }
        mAdapter = new RecyclerViewAdapter(getBaseContext(), activity, Products.products, this::onClickTextView, true, false);
        recyclerView.setAdapter(mAdapter);
//        mFirebaseDatabase.addChildEventListener(valueEventListener);

    }

    View clickedView;


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(v.getId(), MENU_BUTTON_ORDER, 0, "Заказать");
        menu.add(v.getId(), MENU_BUTTON_BOX, 0, "Добавить в корзину");
        clickedView = v;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        View view = clickedView;
        ImageView image = view.findViewById(R.id.itemIconView);
        Bitmap btm = ((BitmapDrawable) image.getDrawable()).getBitmap();
        String name = String.valueOf(((TextView) view.findViewById(R.id.itemName)).getText());
        String price = String.valueOf(((TextView) view.findViewById(R.id.priceTextView)).getText());
        String productId = String.valueOf(((TextView) view.findViewById(R.id.idTextView)).getText());

        int id = item.getItemId();

        switch (id) {
            case MENU_BUTTON_ORDER:
                fbLog.log(TAG, String.format("Item %s with price %s MDL is being to be ordered", name, price));
                Intent intent = new Intent(ActivityShop.this, Order_Activity.class);
                intent.putExtra("price", price);
                intent.putExtra("bitmap", btm);
                intent.putExtra("name", name);
                startActivity(intent);
                break;
            case MENU_BUTTON_BOX:
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user == null)
                    throw new NullPointerException("User is not found");

                DatabaseReference ordersBox = FirebaseDatabase.getInstance().getReference("USERS").child(user.getDisplayName() + ":" + user.getUid()).child("box").child(productId);
                HashMap<String, Object> product = findOrder(productId);
                product.put("image", null);
                ordersBox.updateChildren(product);
                fbLog.log(TAG, String.format("Item %s with price %s MDL added in box", name, price));
                Toast toast = Toast.makeText(getApplicationContext(), String.format("Товар %s успешно добавлен в корзину", name), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM, 0, 100);
                boolean rt = super.onContextItemSelected(item);
                toast.show();
                return rt;
        }
        return super.onContextItemSelected(item);
    }

    public HashMap<String, Object> findOrder(String id) {
        if (Products.products.size() == 0)
            throw new NoSuchElementException("Products collection is empty");

        List<HashMap<String, Object>> found = Products.products.stream().filter(p -> p.get("id").toString().equals(id)).collect(Collectors.toList());
        if (found.size() == 0) {
            throw new NoSuchElementException(String.format("Product with id %s is not found", id));
        } else {
            return found.get(0);
        }
    }


}