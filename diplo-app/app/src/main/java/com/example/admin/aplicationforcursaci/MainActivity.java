package com.example.admin.aplicationforcursaci;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import common.AbstractAction;
import common.Products;

import static common.AbstractAction.getImageUri;
import static common.AbstractAction.toMap;

public class MainActivity extends Activity {

    // Время в милесекундах, в течение которого будет отображаться Splash Screen
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    public static Thread downloadTh = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(() -> {

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getBaseContext());
            if (acct == null) {
                Intent mainIntent = new Intent(MainActivity.this, AuthActivity.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            } else {

                DatabaseReference mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
                ChildEventListener valueEventListener = new ChildEventListener() {

                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        Map<String, HashMap> products = new HashMap<>();
                        Stream.of(dataSnapshot).forEach(a -> products.put(a.getKey(), toMap(a.getValue())));

                        //Show collectionOfProducts
                        if (products.get("PRODUCTS") != null) {
                            downloadTh = new Thread(() -> {


                                List<HashMap<String, Object>> l = Arrays.stream(Objects.requireNonNull(products.get("PRODUCTS")).values().toArray()).map(AbstractAction::toMap).collect(Collectors.toList());

                                for (HashMap<String, Object> innerMap : l.stream().map(AbstractAction::toMap).collect(Collectors.toList())) {
                                    innerMap.forEach((a, b) -> Products.products.add(toMap(b)));
                                }

                                for (int i = 0; i < Products.products.size(); i++) {
                                    String image = Objects.requireNonNull(Products.products.get(i).get("image_name")).toString();

                                    try {
                                        Products.products.get(i).put("image", Glide.with(getBaseContext()).asBitmap().
                                                load(getImageUri(image)).into(250, 250).get());
                                    } catch (ExecutionException | InterruptedException e) {
                                        throw new RuntimeException(e);
                                    }
                                }


                            });
                            downloadTh.start();
                        }

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                };

                mFirebaseDatabase.addChildEventListener(valueEventListener);


                Intent mainIntent = new Intent(MainActivity.this, HamburgerList.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            }
            // По истечении времени, запускаем главный активити, а Splash Screen закрываем

        }, SPLASH_DISPLAY_LENGTH);
    }
}
