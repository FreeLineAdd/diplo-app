package common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.admin.aplicationforcursaci.FirebaseLogger;
import com.example.admin.aplicationforcursaci.Order_Activity;
import com.example.admin.aplicationforcursaci.R;
import com.example.admin.aplicationforcursaci.UserBoxActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static common.AbstractAction.getImageUri;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    private List<HashMap<String, Object>> products;
    private View.OnClickListener listener;
    private Context context;
    private Activity activity;
    private boolean regForContextMenu;
    private boolean shouldButtonBeVisible;

    public RecyclerViewAdapter(Context context, Activity activity, List<HashMap<String, Object>> products, View.OnClickListener listener, boolean regForContextMenu, boolean shouldButtonBeVisible) {
        this.products = products;
        this.listener = listener;
        this.context = context;
        this.activity = activity;
        this.regForContextMenu = regForContextMenu;
        this.shouldButtonBeVisible = shouldButtonBeVisible;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item, parent, false);
        return new ViewHolder(view);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        FirebaseLogger fbLog;
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            fbLog = new FirebaseLogger(currentUser);
        } else throw new NullPointerException("user was not found");
        Log.d(TAG, "onBindViewHolder: called.");
        if (products.get(i).get("image") != null) {
            viewHolder.icon.setImageBitmap((Bitmap) Products.products.get(i).get("image"));
        } else {
            Glide.with(context).asBitmap().
                    load(getImageUri(products.get(i).get("image_name").toString())).into(viewHolder.icon);
        }

        viewHolder.id.setText(Objects.requireNonNull(products.get(i).get("id")).toString());
        viewHolder.name.setText(Objects.requireNonNull(products.get(i).get("name")).toString());
        viewHolder.price.setText(Objects.requireNonNull(products.get(i).get("price")).toString());
        if (regForContextMenu) {
            activity.registerForContextMenu(viewHolder.parentLayout);
            viewHolder.parentLayout.setOnClickListener(listener);
        }
        if (shouldButtonBeVisible) {
            View.OnClickListener onClickListener = view -> {
                View viewParent = (View) view.getTag();
                ImageView image = viewParent.findViewById(R.id.itemIconView);
                Bitmap btm = ((BitmapDrawable) image.getDrawable()).getBitmap();
                String name = String.valueOf(((TextView) viewParent.findViewById(R.id.itemName)).getText());
                String price = String.valueOf(((TextView) viewParent.findViewById(R.id.priceTextView)).getText());
                String productId = String.valueOf(((TextView) viewParent.findViewById(R.id.idTextView)).getText());

                switch (view.getId()) {
                    case R.id.buttonOrder:
                        Intent orderIntent = new Intent(context, Order_Activity.class);
                        orderIntent.putExtra("price", price);
                        orderIntent.putExtra("bitmap", btm);
                        orderIntent.putExtra("name", name);
                        fbLog.log(TAG, String.format("Item %s with price %s MDL is being to be ordered", name, price));
                        activity.startActivity(orderIntent);

                        break;
                    case R.id.deleteFromBox:
                        FirebaseDatabase.getInstance().getReference().child("USERS").child(currentUser.getDisplayName() + ":" + currentUser.getUid()).child("box")
                                .child(productId).removeValue();
                        fbLog.log(TAG, String.format("Item %s with price %s MDL was removed from box", name, price));
                        Intent boxIntent = new Intent(context, UserBoxActivity.class);
                        activity.startActivity(boxIntent);
                        activity.finish();
                        break;

                }
            };
            viewHolder.orderButton.setVisibility(View.VISIBLE);
            viewHolder.orderButton.setOnClickListener(onClickListener);
            viewHolder.addToBoxButton.setVisibility(View.VISIBLE);
            viewHolder.addToBoxButton.setOnClickListener(onClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;
        private TextView id;
        private TextView name;
        private TextView price;
        private RelativeLayout parentLayout;
        private Button orderButton;
        private Button addToBoxButton;

        public ViewHolder(View itemView) {
            super(itemView);
            orderButton = itemView.findViewById(R.id.buttonOrder);
            addToBoxButton = itemView.findViewById(R.id.deleteFromBox);
            id = itemView.findViewById(R.id.idTextView);
            icon = itemView.findViewById(R.id.itemIconView);
            name = itemView.findViewById(R.id.itemName);
            price = itemView.findViewById(R.id.priceTextView);
            parentLayout = itemView.findViewById(R.id.parentLayout);

            orderButton.setTag(parentLayout);
            addToBoxButton.setTag(parentLayout);
        }
    }
}