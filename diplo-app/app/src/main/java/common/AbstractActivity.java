package common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.admin.aplicationforcursaci.FirebaseLogger;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("Registered")
public class AbstractActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    public RecyclerView.Adapter mAdapter;
    public Activity activity;
    public FirebaseLogger fbLog;

    public Map<String, HashMap> products = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isNetworkAvailable())
            Toast.makeText(getBaseContext(), "Failed to get internet connection", Toast.LENGTH_LONG).show();

    }

    public void onClickTextView(View view) {
        openContextMenu(view);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            throw new NullPointerException("connectivityManager = null");

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void sleep(int m) {
        try {
            Thread.sleep(m);
        } catch (InterruptedException e) {
        }
    }
}
