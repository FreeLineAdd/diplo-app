package common;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;
import java.util.NoSuchElementException;

public class AbstractAction {

    public static HashMap<String, Object> toMap(Object object) {
        if (object == null)
            throw new NullPointerException("Object can not be null");
        return (HashMap<String, Object>) object;
    }

    public static Uri getImageUri(String imgName) {
        Log.d("AbstractAction", "getImageUri: " + imgName);
        if (!imgName.contains("\\")) {
            throw new NoSuchElementException("Folder was not indicated");
        }
        Task<Uri> task = FirebaseStorage.getInstance().getReference().child("products")
                .child(imgName.substring(0, imgName.indexOf("\\"))).child(imgName.substring(imgName.indexOf("\\") + 1))
                .getDownloadUrl().addOnFailureListener(exception -> {
                    throw new NoSuchElementException(String.format("Image %s doesn't exists", imgName));
                });
        while (!task.isComplete()) {
            wait(50);
        }
        Log.d("URI", String.format("The file \"%s\" has url %s", imgName, task.getResult()));
        return task.getResult();
    }

    public static void wait(int miliSeconds) {
        try {
            Thread.sleep(miliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
