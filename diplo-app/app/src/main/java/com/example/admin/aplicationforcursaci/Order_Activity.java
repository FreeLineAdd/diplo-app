package com.example.admin.aplicationforcursaci;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import common.AbstractActivity;

public class Order_Activity extends AbstractActivity {
    private static final String TAG = "Order_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_);

        if (!isNetworkAvailable())
            Toast.makeText(getBaseContext(), "Failed to get internet connection", Toast.LENGTH_SHORT).show();

        String name = String.valueOf(getIntent().getCharSequenceExtra("name"));
        String price = String.valueOf(getIntent().getCharSequenceExtra("price"));
        Bitmap btm = getIntent().getParcelableExtra("bitmap");

        RelativeLayout rl = findViewById(R.id.parentLayout);
        rl.setOnClickListener(null);
        ImageView imageView = findViewById(R.id.itemIconView);
        TextView nameView = findViewById(R.id.itemName);
        TextView priceView = findViewById(R.id.priceTextView);

        imageView.setImageBitmap(btm);
        nameView.setText(name);
        priceView.setText(price);
    }


    public void onButtonOrderClick(View view) {
        EditText editText1 = findViewById(R.id.editText);       //Имя
        EditText editText2 = findViewById(R.id.editText5);      //мобильный тлф
        EditText editText3 = findViewById(R.id.editText6);      //Город
        EditText editText4 = findViewById(R.id.editText7);      //Улица
        EditText editText5 = findViewById(R.id.editText8);      //Почтовый индекс
        EditText editText6 = findViewById(R.id.editText9);      //Количество


        String ed1, ed2, ed3, ed4, ed5, ed6;
        ed1 = editText1.getText().toString();
        ed2 = editText2.getText().toString();
        ed3 = editText3.getText().toString();
        ed4 = editText4.getText().toString();
        ed5 = editText5.getText().toString();
        ed6 = editText6.getText().toString();


        if (ed1.length() > 0 && ed2.length() > 9 && ed3.length() > 0 && ed4.length() > 0 && ed5.length() > 0 && ed6.length() > 0) {
            Toast toast = Toast.makeText(getApplicationContext(), "Ваш заказ успешно оформлен", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 150);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Заказ не оформлен, введите коректную информацию", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 150);
            toast.show();
        }
    }

    public void onButtonCancelClick(View view) {
        finish();
    }

}
