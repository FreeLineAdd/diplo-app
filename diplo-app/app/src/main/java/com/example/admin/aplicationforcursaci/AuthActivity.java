/*
 * Copyright 2016 Google LLC.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.admin.aplicationforcursaci;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import common.AbstractActivity;

/*
 * Main activity to select a channel and exchange messages with other users
 * The app expects users to authenticate with Google ID. It also sends user
 * activity logs to a servlet instance through Firebase.
 */
public class AuthActivity extends AbstractActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnKeyListener,
        View.OnClickListener {

    // Firebase keys commonly used with backend servlet instances
    private static final String IBX = "inbox";
    private static final String REQLOG = "requestLogger";

    private static final int RC_SIGN_IN = 9001;

    private static final String TAG = "PlayActivity";
    private static final String INBOX_KEY = "INBOX_KEY";
    private static final String FIREBASE_LOGGER_PATH_KEY = "FIREBASE_LOGGER_PATH_KEY";
    public static FirebaseLogger fbLog;

    public static GoogleApiClient mGoogleApiClient;
    private static String firebaseLoggerPath;
    private static String inbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        if (!isNetworkAvailable())
            Toast.makeText(getBaseContext(), "Failed to get internet connection", Toast.LENGTH_SHORT).show();

        GoogleSignInOptions.Builder gsoBuilder =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail();


        GoogleSignInOptions gso = gsoBuilder.build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);
        Button signOutButton = findViewById(R.id.sign_out_button);
        signOutButton.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "Google authentication status: " + result.getStatus().getStatusMessage());
            // If Google ID authentication is successful, obtain a token for Firebase authentication.
            if (result.isSuccess() && result.getSignInAccount() != null) {
                AuthCredential credential = GoogleAuthProvider.getCredential(
                        result.getSignInAccount().getIdToken(), null);
                FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener(this, task -> {
                            Log.d(TAG, "signInWithCredential:onComplete Successful: " + task.isSuccessful());
                            if (task.isSuccessful()) {
                                final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                                if (currentUser != null) {
                                    inbox = "client-" + Math.abs(currentUser.getUid().hashCode());
                                    requestLogger(() -> {
                                        Log.d(TAG, "onLoggerAssigned logger id: " + inbox);
                                        fbLog.log(inbox, "Signed in");
                                    });
                                }
                            } else {
                                Log.w(TAG, "signInWithCredential:onComplete", task.getException());
                            }
                        });
            } else if (result.getStatus().isCanceled()) {
                String message = "Google authentication was canceled. "
                        + "Verify the SHA certificate fingerprint in the Firebase console.";
                Log.d(TAG, message);
                showErrorToast(new Exception(message));
            } else {
                Log.d(TAG, "Google authentication status: " + result.getStatus().toString());
                showErrorToast(new Exception(result.getStatus().toString()));
            }

            if (result.isSuccess() && result.getSignInAccount() != null) {
                AuthActivity.this.startActivity(new Intent(AuthActivity.this, HamburgerList.class));
                AuthActivity.this.finish();
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                // Start authenticating with Google ID first.
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        inbox = savedInstanceState.getString(INBOX_KEY);
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            fbLog = new FirebaseLogger(currentUser);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }


    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    // [START request_logger]
    /*
     * Request that a servlet instance be assigned.
     */
    public static void requestLogger(final LoggerListener loggerListener) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(IBX + "/" + inbox).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists() && snapshot.getValue(String.class) != null) {
                    fbLog = new FirebaseLogger(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                    databaseReference.child(IBX + "/" + inbox).removeEventListener(this);
                    loggerListener.onLoggerAssigned();
                }
            }

            public void onCancelled(@NonNull DatabaseError error) {
                Log.e(TAG, error.getDetails());
            }
        });


    }


    private void showErrorToast(Exception e) {
        runOnUiThread(
                () -> Toast.makeText(
                        getApplicationContext(),
                        e.getLocalizedMessage(),
                        Toast.LENGTH_LONG).show()
        );
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }


    /**
     * A listener to get notifications about server-side loggers.
     */
    public interface LoggerListener {
        /**
         * Called when a logger has been assigned to this client.
         */
        void onLoggerAssigned();
    }
}
